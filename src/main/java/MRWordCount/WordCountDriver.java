package MRWordCount;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.util.Tool;

public class WordCountDriver extends Configured implements Tool {
    @Override
    public int run(String[] args) throws Exception {
        if(args.length < 2){
            System.out.println("Please enter valid inputs");
            return -1;
        }

        JobConf jobConf = new JobConf(WordCountDriver.class);
        jobConf.setJobName("Word Count Job"); //Setting the job name

        FileInputFormat.setInputPaths(jobConf, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobConf, new Path(args[1]));

        jobConf.setMapperClass(WordCountMapper.class);
        jobConf.setReducerClass(WordCountReducer.class);
        jobConf.setMapOutputKeyClass(Text.class);
        jobConf.setMapOutputValueClass(IntWritable.class);
        jobConf.setOutputKeyClass(Text.class);
        jobConf.setOutputValueClass(IntWritable.class);

        JobClient.runJob(jobConf);

        return 0;

    }
}
