package MRWordCount;

import org.apache.hadoop.util.ToolRunner;

public class Main {

    public static void main(String[] args) throws Exception {

        int exitStatus = ToolRunner.run(new WordCountDriver(), args);
        System.out.println("Program returned " + exitStatus);
    }
}
