package MRWordCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.Iterator;

public class WordCountReducer extends MapReduceBase implements Reducer {
    @Override
    public void reduce(Object key, Iterator values,
                       OutputCollector output, Reporter reporter) throws IOException {

        int wordCount = 0;

        while(values.hasNext()){
            IntWritable i = (IntWritable) values.next();
            wordCount += i.get();
        }

        output.collect(key, new IntWritable(wordCount));
    }
}
